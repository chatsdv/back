const express = require('express');
const messageController = require('../controllers/messageController');

const router = express.Router();

// Route pour cr�er un nouveau message
router.post('/', messageController.createMessage);

// Route pour r�cup�rer tous les messages
router.get('/', messageController.getAllMessages);

module.exports = router;