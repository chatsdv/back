    const Message = require('../models/messageModel');
const { PubSub } = require('@google-cloud/pubsub');

const pubsub = new PubSub({
    projectId: 'leprojetsubv',
    keyFilename: './leprojetsubv-03afe917999e.json'
});
const topic = pubsub.topic("victor");
const subscriptionName = 'victor-sub' 
const subscription =  topic.subscription(subscriptionName);



//cr�er un nouveau message
function createMessage(req, res) {
    const message = req.body.message;
    if (message !== null) {
        topic.publishMessage({
            data: Buffer.from(message.content),
            attributes: { nom: message.sender }
        });
        res.status(201).json(message);
    }
}

// Middleware pour r�cup�rer tous les messages
function getAllMessages(req, res) {
    const messages = [];

    subscription.on('message', message => {
        messages.push(message.data.toString());
        message.ack(); 
    });

    subscription.on('error', error => {
        console.error('Received error:', error);
        res.status(500).json({ error: 'Une erreur est survenue lors de la r�cup�ration des messages' });
    });

    setTimeout(() => {
        res.json({ messages });
    }, 5000); 
}

module.exports = {
  createMessage,
  getAllMessages,
};