
class Message {
    constructor(content, sender) {
        this.content = content;
        this.sender = sender;
    }
}

module.exports = Message;