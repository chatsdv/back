const express = require('express');
const bodyParser = require('body-parser');
const messageRoutes = require('./src/routes/messageRoutes');
const http = require('http');
const app = express();

const server = http.createServer(app);
const io = require('socket.io')(server, {
    cors: {
        origin: '*',
    }
});


const messages = {};

const PORT = process.env.PORT || 3000;

// Middleware pour analyser le corps des requ�tes en JSON
app.use(bodyParser.json());

io.on('connection', (socket) => {
 

    socket.on("getMsgs", msg => {
        console.log(msg);
        socket.emit("messages", messages);
    });

    socket.on("addMsg", msg => {
        messages.push(msg);
        io.emit("messages", Object.keys(messages));
        socket.emit("message", msg);
    });

    io.emit("messages", Object.keys(messages));

    console.log(`Socket ${socket.id} has connected`);
});

// Route principale
app.get('/', (req, res) => {
    res.send('API Node.js fonctionnelle');
});



// Routes des messages
app.use('/messages', messageRoutes);

// D�marrage du serveur
app.listen(PORT, () => {
    console.log(`Serveur en cours d'ex�cution sur le port ${PORT}`);
});