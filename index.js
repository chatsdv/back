// Imports the Google Cloud client library
const { PubSub } = require('@google-cloud/pubsub');

function mainPubSub() {

    // Instantiates a client
    const pubsub = new PubSub({
        projectId: 'leprojetsubv',
        keyFilename: './leprojetsubv-03afe917999e.json'
    });

    const topic = pubsub.topic("testtopic");

    subscription.on('message', message => {
        console.log('Received message:', message.data.toString());
        process.exit(0);
    });

    // Receive callbacks for errors on the subscription
    subscription.on('error', error => {
        console.error('Received error:', error);
        process.exit(1);
    });

    topic.publishMessage({
        data: Buffer.from('Victor Message!'),
        attributes: { nom: "Victor" }
    });
}

mainPubSub();